import React, { Component } from 'react'
import { Skeleton, Switch, Card, Icon, Avatar } from 'antd';
import { Menu, Dropdown } from 'antd';

const { Meta } = Card;

const menu = (
  <Menu>
    <Menu.Item key="0">
      <a target="_blank" rel="noopener noreferrer" href="http://www.alipay.com/">1st menu item</a>
    </Menu.Item>
    <Menu.Item key="1">
      <a target="_blank" rel="noopener noreferrer" href="http://www.taobao.com/">2nd menu item</a>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="3" disabled>3rd menu item（disabled）</Menu.Item>
  </Menu>
);
export class CardLoad extends Component {
  state = {
    loading: true,
  }
  onChange = (checked) => {
    this.setState({ loading: !checked });
  }


  render() {
    const { loading } = this.state;

    return (

      <div>
        <Switch checked={!loading} onChange={this.onChange} />

        <Card style={{ marginTop: 16 }} loading={loading}>
          <Meta
            avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
            title="Card title"
            description="This is the description"
          />
        </Card>

        <Card
          style={{ marginTop: 16 }}
          actions={[<Icon type="setting" />, <Icon type="edit" />, <Icon type="ellipsis" />]}
        >
          <Skeleton loading={loading} avatar active>
            <Meta
              avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
              title="Card title"
              description="This is the description"
            />
          </Skeleton>
        </Card>
        <Card
          style={{ marginTop: 16 }}
          cover={
            <div className="col-md-12">
              <div className="row">
                <div className="3">
                  <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                </div>
                <div className="6">
                  asd
          </div>
              </div>
            </div>
          }
          actions={[<Icon type="setting" />, <Icon type="edit" />, <Icon type="ellipsis" />]}
          extra={<Dropdown overlay={menu}>
          <a className="ant-dropdown-link">
            Hover me <Icon type="down" />
          </a>
        </Dropdown>}
        >
          <Meta
            avatar={

              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />

            }
            title="Card title"
            description="This is the description"
          />
        </Card>
      </div>
    );

  }
}

export default CardLoad
