import React, { Component } from 'react'
import { Button } from 'antd';
import { Link } from 'react-router-dom'
import { Menu, Icon, Badge } from 'antd';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

export class Navbar extends Component {

  state = {
    current: 'mail',
  }

  handleClick = (e) => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  }

  render() {

    return (
      <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor02">
          <ul class="navbar-nav mr-auto">
            {/* <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Pricing</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">About</a>
              </li> */}
          </ul>
          {this.props.logeado === 0 ?
            <ul class="navbar-nav my-2 my-lg-0">
              <li class="nav-item ant-menu-item">
                <Link to="/login"><Button>Iniciar Sesión</Button></Link>
              </li>

              <li class="nav-item">
                <Link to="/register"><Button>Registrate</Button></Link>
              </li>
            </ul>
            : <ul class="navbar-nav my-2 my-lg-0">

              <Menu
                onClick={this.handleClick}
                selectedKeys={[this.state.current]}
                mode="horizontal"
              >
                <Menu.Item key="mail">
                  <Badge count={5}>
                    <Link to="/mensajes"><Icon type="mail" />Mensajes</Link>
                  </Badge>
                </Menu.Item></Menu>
              <li class="nav-item">
                <Menu
                  onClick={this.handleClick}
                  selectedKeys={[this.state.current]}
                  mode="horizontal"
                  theme="dark"
                >
                  <SubMenu title={<span><Icon type="setting" />{'Luis Antonio Padre Garcia'}</span>}>
                    <MenuItemGroup title="">
                      <Menu.Item key="setting:1"><Link to="/perfil">Perfil</Link></Menu.Item>
                      <Menu.Item key="setting:2"><Link to="/Configuraciones">Configuraciones</Link></Menu.Item>
                      <Menu.Item key="setting:3"><a onClick={this.props.salir} >Cerrar Sesión</a></Menu.Item>
                    </MenuItemGroup>
                    <MenuItemGroup title="">
                      <Menu.Item key="setting:4"><Link to="/Ayuda">Ayuda</Link></Menu.Item>
                      <Menu.Item key="setting:5"><Link to="/sugerencias">Enviar sugerencias</Link></Menu.Item>
                      <Menu.Item key="setting:6">Idioma:Español</Menu.Item>
                    </MenuItemGroup>
                  </SubMenu>
                </Menu>
              </li>
            </ul>
          }

        </div>
      </nav>

    )
  }
}

export default Navbar
