import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class MessageList extends Component {
  static propTypes = {

  }

  render() {
      
    return (

      <ul className="message-list">
            {this.props.messages.map(message => {
                return (
                        <div>
                    <div key={message.id}>
                            <strong>{message.senderId}:</strong> " "{message.text}
                        </div>
                        
                    </div>
                )
            })}
      </ul>
    )
  }
}

export default MessageList
