import { Menu, Icon } from 'antd';
import React, { Component } from 'react'
import List from 'react-list-select'
import { Avatar } from 'antd';

class menu extends React.Component {


    render() {

        function comp(name, email) {
            return (
                <div className="contact">
                    <div className="row">
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                        <div className="name">{name}</div>
                    </div>
                    <div className="email">{email}</div>
                </div>
            )
        }


        let comps = [
            comp('Mike', 'mike@server.com'),
            comp('John', 'john@server.com'),
            comp('Bob', 'bob@server.com'),
            comp('Max', 'max@server.com'),
        ]

        let list = (
            <List

                keyboardEvents={false}
                items={comps}
                disabled={[2]}
                selected={[0]}
                onChange={console.log.bind(console)}
            />
        )

        return (

            <div className="col-md-12">
                {list}
            </div>

        )
    }
}

export default menu
