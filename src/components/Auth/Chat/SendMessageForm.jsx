import React, { Component } from 'react'

export class SendMessageForm extends Component {


  render() {
    return (
        <form className="send-message-form">
        <input
          onChange={this.handleChange}
          value={this.props.message}
          placeholder="Type your message and hit ENTER"
          type="text" />

        </form>
    )
  }
}

export default SendMessageForm
