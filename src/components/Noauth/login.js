import React, { Component } from 'react'
import { Form, Icon, Input, Button, Card,Checkbox } from 'antd';

const FormItem = Form.Item;

export class login extends Component {

  render() {

      return (
        <center>
        <Card className="container"  title="Iniciar sesión" bordered={true}>
          
        <Form className="login-form col-md-4">
          <FormItem>
            
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
          </FormItem>
          <FormItem>
          
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
          
          </FormItem>
          <FormItem>
          
              <Checkbox>Remember me</Checkbox>
          
            <a className="login-form-forgot" href="">Forgot password</a>
            <hr/>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Log in
            </Button>
            Or <a href="">register now!</a>
          </FormItem>
        </Form>
        </Card>
        </center>
    )
  }
}

export default login
