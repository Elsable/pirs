import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';  // or 'antd/dist/antd.less'
import Prism from 'prismjs';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
